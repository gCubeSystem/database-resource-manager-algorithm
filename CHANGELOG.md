
# Changelog


## [v1.5.1] [r4.24.0] - 2020-06-10

### Features

- Updated for support https protocol [#19423]

## [v1.5.0] - 2017-02-15

### Features

- Removed ScopeProvider calls

## [v1.4.0] - 2016-12-01

### Features

- Updated algorithms to DataMiner

## [v1.3.0] - 2014-12-05

### Features

- algorithm SubmitQuery modified in order to pass the string pathfile to the submitQuery method of class DatabaseManagement in order to generate a file and retrieve it from the statistical manager with the map as output.
- Algorithms RandomSampleOnTable, SampleOnTable, SmartSampleOnTable and SubmitQuery modified in order to manage the file as output.
- total number rows returned as output for the result of the submit Query operation: algorithm SubmitQuery modified

## [v1.2.0] - 2014-10-27

### Features

- some algorithms modified to cope modifications applied in the database-resource-manager component and regarding the management of the same access point for a resource and the database connection shutdown.

## [v1.1.0] - 2014-09-10

### Features

- SubmitQuery algorithm updated because input parameters in the SubmitQuery method of DatabaseManagement class have been updated
- timer added in the SubmitQuery algorithm that stops the query execution after 30 minutes
- hibernate bug fixed and related to a join tables with same columns names. Bug resolved using JDBC. GetConnection method changed in the SubmitQuery algorithm to create the connection with JDBC
- method shutdown of algorithm SubmitQuery implemented in order to allow a user to cancel a submit query operation
- bug fixed when two algorithms are submitted concurrently.Bug related to map result's name. Bug fixed generating the name as ResultsMap"+UUID.randomUUID()
- classes SubmitQuery, SampleOnTable, SmartSampleOnTable, RandomSampleOnTable modified in order to not make available the file as result
- file DestinationDBHibernate modified to insert the property on the timeout
- bug fixed on table names that are keywords.Method GetTableDetails corrected to consider the name as schema.table for postgres and database.table for mysql

## [v1-0-0] - 2014-06-09

### Features

- First Release


This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).